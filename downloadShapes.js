import fs from 'fs';
import request from 'request';

let listShapeFiles = new Map () ;

function loadConfig() {
    let config= JSON.parse( fs.readFileSync ('shapes.json'));
    config.map (  line => {   listShapeFiles.set (Object.keys(line)[0],Object.values(line)[0]) })
    console.log(listShapeFiles)
  }

  async function loadShapes () {
    listShapeFiles.forEach((value,key)=>{
      request(value,  {json: false}, (error, res, body)   =>   {
        if ( res.statusCode == 200) {
          console.log ("write file " +  './SHACL/'+key+'.ttl');
           fs.writeFileSync( './SHACL/'+key+'.ttl', body )
        }
      })
    })
  }

  function main() {
    loadConfig();
    loadShapes();
  }

  main();