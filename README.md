# Schema-validator

## Purpose

The goal of this component is to check if a JSON is compliant with a SHACL (Shapes). <br/>
The component have a configured list of SHACL to check a participant, a locatedServiceOffering, a serviceOffering, a provider ... <br/>
SHCAL are downloaded from the schema provider of the federation.

## Installation

1. Get node-js (>= 16.0.0)
2. Get npm
3. Install dependencies run `npm install`

## Start

`npm start`

## Example using REST

`curl -X POST http://localhost:8080/participant -H "Content-Type: application/json" -d @./data/provider-ko.json`

`curl -X POST https://schemavalidator.abc-federation.gaia-x.community/provider -H "Content-Type: application/json" -d @./data/provider-ko.json`
(ne fonctionne pas pour le moment a cause d'un pb de certificat)

`curl -X POST http://localhost:8080/serviceOffering -H "Content-Type: application/json" -d @./data/provider-ko.json`

## Command line xemples

- to download shapes or update shapes : node download.js 

- working sample : node cmd.js ./SHACL/providerShape.ttl ./data/provider-ok.json
- non working sample : node cmd.js ./SHACL/providerShape.ttl ./data/provider-ko.json

# Input / Output

## input

Any valid JSON in the http body

and a shape to check the json. 
Accepted values:
- participant
- serviceOffering
- location
- locatedServiceOffering
- provider

Theses shapes are related to schemas provided at theses url : https://schemas.abc-federation.gaia-x.community/wip/validation/

## output

in output :
```
{
    "statut": "true" // or false
    "details" : [] // optionnaly provide some details about an error
    "message" : "error" //optionnaly if an error occur
    "reason" : "error" // optionnaly an error message
}
```

## shapes updates

Shapes are set in cache in the process each x. 
x is set 
- in command line using node index.js --LOAD_SHAPES_EACH 500 to relaod shapes each 500 ms. A normal value is high like 500000
- but an environnement variable :           
    - name: LOAD_SHAPES_EACH
    value: "500000"
to download shapes each 500000ms

# Define shapes 

a configuration file [shapes.json](shapes.json) is provided with a list of shape : url 
like this file:
```
[
    { "participant" : "https://schemas.abc-federation.gaia-x.community/wip/validation/participantShape.ttl"},                        
    { "serviceOffering" : "https://schemas.abc-federation.gaia-x.community/wip/validation/service-offeringShape.ttl"},
    { "location" :"https://schemas.abc-federation.gaia-x.community/wip/validation/locationShape.ttl" },
    { "locatedServiceOffering" : "https://schemas.abc-federation.gaia-x.community/wip/validation/located-service-offeringShape.ttl"},
    { "provider" : "https://schemas.abc-federation.gaia-x.community/wip/validation/providerShape.ttl "}
]
```

## Download shapes locally 

To use the component locally, SHACL MUST be in local. 

To download shapes execute:
```
node downloadShapes.js 
```
