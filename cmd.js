/*
    command line tool to check a json file
    usage : 
            node cmd.js shapefile json file
    exemple : 
   -  working sample : node cmd.js ./SHACL/providerShape.ttl ./data/provider-ok.json
   -  non working sample : node cmd.js ./SHACL/providerShape.ttl ./data/provider-ko.json

   to get shacl updated run : node downloadShapes.js before to use the tool to get defined shapes (shapes.json)
*/

import fs from 'fs';
import factory from 'rdf-ext';
import ParserN3 from '@rdfjs/parser-n3';
import ParserJsonld from '@rdfjs/parser-jsonld'
import SHACLValidator from 'rdf-validate-shacl';
import { Console } from 'console';

async function loadDataset (filePath) {
  const stream = fs.createReadStream(filePath)
  const parser = new ParserN3({ factory })
  return factory.dataset().import(parser.import(stream))
}

async function loadJSONLDDataset (filePath) {
  const stream = fs.createReadStream(filePath)
  const parserJsonld = new ParserJsonld()
  return factory.dataset().import(parserJsonld.import(stream))
}

async function main() {
  const myArgs = process.argv.slice(2);
  const shaclShapeGraph = myArgs[0];
  const dataGraph = myArgs[1];
  console.log('Validate data graph '+dataGraph+' using shape graph '+shaclShapeGraph);
  const shapes = await loadDataset(shaclShapeGraph);
  const data = await loadJSONLDDataset(dataGraph);
  const validator = new SHACLValidator(shapes, { factory })
  const report = await validator.validate(data)

  // Check conformance: `true` or `false`
  console.log('Conformity :'+report.conforms)

  console.log (report);
  if (report.conforms == false) {
    for (const result of report.results) {
      // See https://www.w3.org/TR/shacl/#results-validation-result for details
      // about each property
      console.log('Print report results...\n')
      console.log(result.message)
      console.log(result.path)
      console.log(result.focusNode)
      console.log(result.severity)
      console.log(result.sourceConstraintComponent)
      console.log(result.sourceShape)
    }

    // Validation report as RDF dataset
    console.log('Print report dataset...')
    console.log(report.dataset)
  }
}

main();
