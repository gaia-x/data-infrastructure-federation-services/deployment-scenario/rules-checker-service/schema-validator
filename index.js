import express from 'express';
import fs from 'fs';
import factory from 'rdf-ext';
import ParserN3 from '@rdfjs/parser-n3';
import ParserJsonld from '@rdfjs/parser-jsonld'
import SHACLValidator from 'rdf-validate-shacl';
import Readable from 'stream' 
import request from 'request';
import nconf from 'nconf';
import  jsonld from 'jsonld';

nconf.env(). argv();
const port = 8080;
const app = express()
app.use(express.json());

const parser = new ParserN3({ factory })
let shapesFiles = new Map(); 
let listShapeFiles = new Map () ;

// gere les erreurs pour retourner un json avec statut:false
app.use((error, request, response, next) => {
      response.status(500).send({"statut": "false" , "message" : JSON.stringify (error)});
      next();
});

// logue les erreurs sur la console.
app.use((error, request, response, next) => {
  console.error(error) // or using any fancy logging library
      next(error);
});

app.get('/healthcheck', (req, res) => {
  res.sendStatus(200);
  res.end();
})

async function loadShapeToGraph (shape) {
  var s = new Readable.Readable()
  s.push(shape)   
  s.push(null)    
  return  factory.dataset().import(parser.import(s) );
}

async function loadShapes () {
  listShapeFiles.forEach((value,key)=>{
    request(value,  {json: false}, (error, res, body)   =>   {
      if (error) {
          console.log ("critical error while loading " + key + ' ' + value + ' ' + error);
          if (shapesFiles.get(key)==undefined) { // if a shape is unavailable and not previously loaded then exit, if already loaded the process will continue to use the previously loaded shape
            console.log ('Error while reloading shape ' + key + ' with error ' + error ) ;
            //process.exit(1);
          }
      }
      if (!error && res.statusCode == 200) {
        loadShapeToGraph (body).then (fact => {    shapesFiles.set (key,fact);  })
      }})
    })
  }

async function loadJSONLDDataset (json) {
  try { 
    const parserJsonld = new ParserJsonld()
    var s = new Readable.Readable()
    s.push(JSON.stringify(json))   
    s.push(null)    
    const output = parserJsonld.import( s);
    return factory.dataset().import(output);
} catch (error) {
    let exception = { "statut":"false" , "reason" : error.cause , "message" : error.message}
    console.log ("exception " + loadJSONLDDataset + " error " +  error.message);
    return exception;
}}

async function  check (shape, json) {
  try { 
    let  shapes = shapesFiles.get(shape);
    const data = await loadJSONLDDataset(json);
    const validator = new SHACLValidator(shapes, { factory })
    const report = await validator.validate(data)
    let explain = [];
    for (const result of report.results) {
        explain.push( {  "message" : result.message , "path" : result.path , "shape" : result.sourceShape     })
      } 
    return { "statut": report.conforms };
} catch (error) {
  return  { "statut":"false" , "reason" : error.cause , "message" : error.message};
}}

 app.post('/:shapeId', (req, res) => {
  console.log ("check for shape " + req.params.shapeId);
  check (req.params.shapeId,  req.body).then (report => {
    res.json (JSON.stringify(report));
    res.end(); 
  })
})

async function normalize (vc) {
  let doc = vc.credentialSubject;
  let context = vc['@context'];
  let normalized = await  jsonld.normalize(doc, {algorithm: 'URDNA2015', expansion: false});
  vc.credentialSubject=normalized;
  return vc;
}

async function compact (vc) {
  let doc = vc.credentialSubject;
  let context = vc['@context'];
  console.log (context);
  let compacted = await  jsonld.compact(doc, {algorithm: 'URDNA2015', expansion: false});
  return compacted;
}
app.post('/format/compact', (req, res) => {
  console.log ("body : " + JSON.stringify(req.body));
  compact(req.body).then (res=> {
          res.json (JSON.stringify(js));
          res.end(); 
})
})

app.get('/ttl/:ttl', (req, res) => {
  console.log ("get shape " + req.params.ttl);
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.send (fs.readFileSync('./TTL/' + req.params.ttl).toString());
  res.end(); 
})

function loadConfig() {
  let config= JSON.parse( fs.readFileSync ('shapes.json'));
  config.map (  line => {   listShapeFiles.set (Object.keys(line)[0],Object.values(line)[0]) })
  console.log(listShapeFiles)
}

function start () {
  app.listen(port, () => {
    loadConfig();
    loadShapes();
    console.log ('reload shapes each  ' + nconf.get ('LOAD_SHAPES_EACH'));
    setInterval(loadShapes, nconf.get ('LOAD_SHAPES_EACH')); 
    console.log(`schema validator listening on port ${port}`)
  })
  /*
  compact(fs.readFileSync('./TTL/test2.jsonld')).then (res=> {
    res.json (JSON.stringify(js));
    res.end(); 
  })*/ 
}
start();